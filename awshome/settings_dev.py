from .settings_common import *

DEBUG = True
ALLOWED_HOSTS = []
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,

    #ロガー
    'loggers':{
        'django':{
            'handlers':['console'],
            'level':'INFO',
        },

        'home':{
            'handlers':['console'],
            'level':'DEBUG',
        },
    },
    #ハンドラ
    'handlers':{
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter':'dev'
        },
    },

    #フォーマッタ
    'formatters':{
        'dev':{
            'format': '\t'.join([
                '%(asctime)s',
                '[%(levelname)s]',
                '%(pathname)s(Line:%(lineno)d)',
                '%(message)s'
            ])
        },
    }
}


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
