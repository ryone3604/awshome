from django.shortcuts import render
from django.views import generic
from . import forms
import logging
from django.urls import reverse_lazy
from django.contrib import messages

from .models import Post, Movie
from django.core.paginator import Paginator


logger = logging.getLogger(__name__)

# Create your views here.



def index(request, num=1):
    date = Movie.objects.all().order_by('published_date').reverse()
    date2 = Post.objects.all().order_by('published_date').reverse()
    page = Paginator(date, 5)
    page2 = Paginator(date2, 4)
    params = {
        'title': 'Top',
        'msg': 'toppagedesu.',
        'date': page.get_page(num),
        'date2': page2.get_page(num)

    }
    return render(request, 'home/top.html', params)


class InquiryView(generic.FormView):
    template_name = "home/contact.html"
    form_class = forms.InquiryForm
    success_url = reverse_lazy('home:contact')

    def form_valid(self, form):
        form.send_email()
        messages.success(self.request,'おくったよ')
        logger.info('Inquiry sent by {}'.format(form.cleaned_data['name']))
        return super().form_valid(form)





def works(request, num=1):
    date = Movie.objects.all().order_by('published_date').reverse()
    page = Paginator(date, 3)
    params = {
        'title': 'Music',
        'msg': 'workpagedesu.',
        'date': page.get_page(num)
    }
    return render(request, 'home/works.html', params)


def works_detail(request, url):
    date = Movie.objects.filter(url=url)
    params = {
        'title': 'Music',
        'msg': 'workpagedesu.',
        'date': date
    }
    return render(request, 'home/works_detail.html', params)


def about(request):
    params = {
        'title': 'About',
        'msg': 'aboutpagedesu.',
    }
    return render(request, 'home/index.html', params)


def blog(request, num=1):
    date = Post.objects.all().order_by('published_date').reverse()
    page = Paginator(date, 3)
    params = {
        'title': 'Blog',
        'msg': 'blogpagedesu.',
        'date': page.get_page(num)
    }
    return render(request, 'home/blog.html', params)


def blog_detail(request, url):
    date = Post.objects.filter(url=url)
    params = {
        'title': 'Blog',
        'msg': 'blogpagedesu.',
        'date': date
    }
    return render(request, 'home/blog_detail.html', params)




