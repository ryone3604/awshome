from django.urls import path
from . import views

app_name = 'home'
urlpatterns = [
    path('', views.index, name="top"),
    path('contact/', views.InquiryView.as_view(), name='contact'),

    path('works/', views.works, name='works'),
    path('works/<int:num>', views.works, name='works'),
    path('works/<str:url>', views.works_detail, name='works_detail'),
    path('about/', views.about, name='about'),

    path('blog/', views.blog, name='blog'),
    path('blog/<int:num>', views.blog, name='blog'),
    path('blog/<str:url>', views.blog_detail, name='blog_detail'),

]